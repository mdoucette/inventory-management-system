package ca.rhythmtech.model;

/**
 * This is the representation of a stock item which contains the data to tie together the Item and Store into
 * a Stock object.
 *
 * @author Mark Doucette,
 */
public class Stock {
    public static final int NUM_STOCK_ELEMENTS = 3;

    private Store store;
    private Item item;
    private int itemCount;

    /**
     * default constructor
     */
    public Stock() {
    }

    /**
     * This is the main constructor for object of Stock that are comprised of both a Store and an associated
     * Item.
     *
     * @param store
     * @param item
     * @param itemCount
     */
    public Stock(Store store, Item item, int itemCount) {
        this.store = store;
        this.item = item;
        this.itemCount = itemCount;
    }

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the itemCount
     */
    public int getItemCount() {
        return itemCount;
    }

    /**
     * @param itemCount the itemCount to set
     */
    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    /**
     * Calculate and return the value of the inventory for this stock object
     *
     * @return the valueCAD
     */
    public float getInventoryValue() {
        return ((float) (item.getPrice()) / 100) * itemCount;
    }

    /**
     * @return an array representation of the Stock
     */
    public Object[] toArray() {
        Object[] itemArray = new Object[Item.NUM_ITEM_ELEMENTS];
        int i = 0;
        itemArray[i++] = store.getID();
        itemArray[i++] = item.getProductNumber();
        itemArray[i++] = itemCount;

        return itemArray;
    }

}
