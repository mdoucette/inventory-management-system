package ca.rhythmtech.model.dao;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.io.StoresReader;
import ca.rhythmtech.model.Database;
import ca.rhythmtech.model.Store;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * This creates a dao object that is specific to Stores
 *
 * @author Mark Doucette,
 */
public class StoreDao extends Dao {
    public static final String TABLE_NAME = "_Stores";
    private static final String STORE_ID = "id";
    private static final String DESCRIPTION = "description";
    private static final String STREET = "street";
    private static final String CITY = "city";
    private static final String PROVINCE = "province";
    private static final String POSTAL_CODE = "postalCode";
    private static final String STORE_PHONE = "storePhone";
    private static final String AUTO_PHONE = "autoPhone";
    private static final String STORES_FILE = "resources/stores.txt";
    public static final String[] columnNames = {STORE_ID, DESCRIPTION, STREET, CITY, PROVINCE, POSTAL_CODE, STORE_PHONE, AUTO_PHONE};
    private static final Logger LOG = Logger.getLogger(StoreDao.class);

    public StoreDao() {
        super(TABLE_NAME);
        init();
    }

    /**
     * Take care of loading / reloading the data
     *
     * @throws SQLException
     * @throws FileNotFoundException
     */
    private void init() {
        Map<String, Store> stores = new HashMap<String, Store>();
        try {
            if (!Database.tableExists(TABLE_NAME)) {
                File storesFile = new File(STORES_FILE);
                if (!storesFile.exists()) {
                    LOG.error("Stores file not found");
                }
                else {
                    stores = StoresReader.read(storesFile);
                    createTable(); // create a new Stores table
                    load(stores); // populate the database with Store rows
                }

            }
        }
        catch (ApplicationException e) {
            LOG.error(e.getMessage());
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see ca.rhythmtech.model.Dao#create()
     */
    @Override
    public void createTable() {
        String sql = String
                .format("CREATE TABLE %s (%s CHAR(5), %s VARCHAR(40), %s VARCHAR(40), %s VARCHAR(30), %s CHAR(2), %s CHAR(7), %s CHAR(14), %s CHAR(14), primary key (%s) )",
                        TABLE_NAME, STORE_ID, DESCRIPTION, STREET, CITY, PROVINCE, POSTAL_CODE, STORE_PHONE, AUTO_PHONE, STORE_ID);
        super.createTable(sql);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ca.rhythmtech.model.Dao#delete()
     */
    void delete(Store store) {
        String sql = String.format("DELETE FROM %s WHERE %s=%s", TABLE_NAME, STORE_ID, store.getID());
        super.delete(sql);

    }

    /**
     * Add an Store to the database.
     *
     * @param Store
     */
    public void create(Store store) {
        String sql = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = null;
        Connection connection = Database.getInstance().getConnection();
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, store.getID());
            statement.setString(2, store.getDescription());
            statement.setString(3, store.getStreet());
            statement.setString(4, store.getCity());
            statement.setString(5, store.getProvince());
            statement.setString(6, store.getPostalCode());
            statement.setString(7, store.getStorePhone());
            statement.setString(8, store.getAutoPhone());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }
    }

    /**
     * Get an Store from the database
     *
     * @return the requested Store
     * @throws ApplicationException
     */
    @SuppressWarnings("resource")
    public Store getStore(String id) throws ApplicationException {
        String sql = String.format("SELECT * FROM %s WHERE %s='%s'", TABLE_NAME, STORE_ID, id);
        Connection connection = Database.getInstance().getConnection();
        Statement statement = null;
        Store store = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            int count = 1;
            while (resultSet.next()) {
                if (count > 1) {

                    throw new ApplicationException("Expected a single row but received " + count); // too many
                    // rows!
                }
                store = new Store();
                store.setID(resultSet.getString(STORE_ID));
                store.setDescription(resultSet.getString(DESCRIPTION));
                store.setStreet(resultSet.getString(STREET));
                store.setCity(resultSet.getString(CITY));
                store.setProvince(resultSet.getString(PROVINCE));
                store.setPostalCode(resultSet.getString(POSTAL_CODE));
                store.setStorePhone(resultSet.getString(STORE_PHONE));
                store.setAutoPhone(resultSet.getString(AUTO_PHONE));

                count++;
            }
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }

        return store;
    }

    /**
     * Update Store in the database
     */
    public void update(Store store) {
        String sql = String.format("update " + TABLE_NAME + " SET id='" + store.getID() + "'," + DESCRIPTION + "='" + store.getDescription() + "', "
                + STREET + "='" + store.getStreet() + "', " + CITY + "='" + store.getCity() + "', " + PROVINCE + "='" + store.getProvince() + "', "
                + POSTAL_CODE + "='" + store.getPostalCode() + ", " + STORE_PHONE + "='" + store.getStorePhone() + "', " + AUTO_PHONE + "='"
                + store.getAutoPhone() + "' where " + STORE_ID + "='" + store.getID() + "'");
        Statement statement = null;
        try {
            statement = Database.getInstance().getConnection().createStatement();
            statement.executeUpdate(sql);
            System.out.println("SQL Update performed");
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }
    }

    /**
     * @return the Stores
     * @throws SQLException
     */
    public Map<String, Store> getStores() throws SQLException {
        Map<String, Store> stores = new HashMap<String, Store>();
        String sql = "SELECT * FROM " + TABLE_NAME;
        Connection connection = Database.getInstance().getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            Store store = new Store();
            store.setID(resultSet.getString(STORE_ID));
            store.setDescription(resultSet.getString(DESCRIPTION));
            store.setStreet(resultSet.getString(STREET));
            store.setCity(resultSet.getString(CITY));
            store.setProvince(resultSet.getString(PROVINCE));
            store.setPostalCode(resultSet.getString(POSTAL_CODE));
            store.setStorePhone(resultSet.getString(STORE_PHONE));
            store.setAutoPhone(resultSet.getString(AUTO_PHONE));
            stores.put(store.getID(), store);
        }
        if (resultSet != null) {
            resultSet.close();
        }
        closeStatement(statement);

        return stores;
    }

    /**
     * Load all of the Stores into the table in the database
     *
     * @param Stores the list of Stores to add to the Stores table
     */
    private void load(Map<String, Store> Stores) {
        if (Stores != null) {
            for (Store Store : Stores.values()) {
                create(Store);
            }
        }
        else {
            LOG.debug("Could not load Stores to database");
        }
    }

    /**
     * Get a 2d array representation of the list/Map for use with the JTable constructor
     *
     * @return 2D array of Stores
     */
    public Object[][] get2DArray() {
        Object[][] data = null;// new Object[Stores.size()][Store.NUM_Store_ELEMENTS];
        Map<String, Store> stores;
        try {
            stores = getStores();
            data = new Object[stores.size()][Store.NUM_STORE_ELEMENTS];
            int i = 0;
            for (Store store : stores.values()) {
                Object[] row = store.toArray();
                data[i++] = row;
            }
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        return data;
    }
}
