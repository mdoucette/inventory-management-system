package ca.rhythmtech.model.dao;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.io.ItemsReader;
import ca.rhythmtech.model.Database;
import ca.rhythmtech.model.Item;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * This creates a dao object that is specific to Items
 *
 * @author Mark Doucette,
 */
public class ItemDao extends Dao {
    public static final String TABLE_NAME = "_Items";
    private static final String DESCRIPTION = "description";
    private static final String PRODUCT_NUMBER = "productNumber";
    private static final String PRICE = "price";
    private static final String DISCOUNT = "discount";
    private static final String IMAGE_NAME = "imageName";
    private static final String ITEMS_FILE = "resources/items.txt";
    public static final String[] columnNames = {"Description", "Product Number", "Price", "Discount", "Image Name"};
    private static final Logger LOG = Logger.getLogger(ItemDao.class);

    public ItemDao() {
        super(TABLE_NAME);
        init();
    }

    /**
     * Take care of loading / reloading the data
     *
     * @throws SQLException
     * @throws FileNotFoundException
     */
    private void init() {
        Map<String, Item> items = new HashMap<String, Item>();
        try {
            if (!Database.tableExists(TABLE_NAME)) {
                File itemsFile = new File(ITEMS_FILE);
                if (!itemsFile.exists()) {
                    LOG.error("Inventory file not found");
                }
                else {
                    items = ItemsReader.read(itemsFile);
                    createTable(); // create a new items table
                    load(items); // populate the database with Item rows
                }

            }
        }
        catch (ApplicationException e) {
            LOG.error(e.getMessage());
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see ca.rhythmtech.model.Dao#create()
     */
    @Override
    public void createTable() {
        String sql = String.format("create table %s (%s VARCHAR(200), %s VARCHAR(60), %s INTEGER, %s FLOAT, %s VARCHAR(40), primary key (%s) )",
                TABLE_NAME, DESCRIPTION, PRODUCT_NUMBER, PRICE, DISCOUNT, IMAGE_NAME, PRODUCT_NUMBER);
        super.createTable(sql);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ca.rhythmtech.model.Dao#delete()
     */
    void delete(Item item) {

        String sql = String.format("DELETE FROM %s WHERE %s=%s", TABLE_NAME, PRODUCT_NUMBER, item.getProductNumber());
        super.delete(sql);

    }

    /**
     * Add an item to the database.
     *
     * @param item
     */
    public void create(Item item) {
        String sql = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?, ?)";
        PreparedStatement statement = null;
        Connection connection = Database.getInstance().getConnection();
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, item.getDescription());
            statement.setString(2, item.getProductNumber());
            statement.setInt(3, item.getPrice());
            statement.setFloat(4, item.getDiscount());
            statement.setString(5, item.getImageName());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }
    }

    /**
     * Get an item from the database
     *
     * @return the requested item
     * @throws ApplicationException
     */
    @SuppressWarnings("resource")
    public Item getItem(String productNumber) throws ApplicationException {
        String sql = String.format("SELECT * FROM %s WHERE %s='%s'", TABLE_NAME, PRODUCT_NUMBER, productNumber);
        Connection connection = Database.getInstance().getConnection();
        Statement statement = null;
        Item item = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            int count = 1;
            while (resultSet.next()) {
                if (count > 1) {

                    throw new ApplicationException("Expected a single row but received " + count); // too many
                    // rows!
                }
                item = new Item();
                item.setDescription(resultSet.getString(DESCRIPTION));
                item.setProductNumber(resultSet.getString(PRODUCT_NUMBER));
                item.setPrice(resultSet.getInt(PRICE));
                item.setDiscount(resultSet.getFloat(DISCOUNT));
                item.setImageName(resultSet.getString(IMAGE_NAME));

                count++;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            closeStatement(statement);
        }

        return item;
    }

    /**
     * Update item in the database
     */
    public void update(Item item) {
        String sql = "UPDATE " + TABLE_NAME + " SET " + DESCRIPTION + "=?, " + PRODUCT_NUMBER + "=?, " + PRICE + "=?, " + DISCOUNT + "=?, "
                + IMAGE_NAME + "=? WHERE " + PRODUCT_NUMBER + "=?";
        PreparedStatement statement = null;
        try {
            statement = Database.getInstance().getConnection().prepareStatement(sql);
            statement.setString(1, item.getDescription());
            statement.setString(2, item.getProductNumber());
            statement.setInt(3, item.getPrice());
            statement.setFloat(4, item.getDiscount());
            statement.setString(5, item.getImageName());
            statement.setString(6, item.getProductNumber());

            statement.executeUpdate();
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }
    }

    /**
     * @return the items
     * @throws SQLException
     */
    public Map<String, Item> getItems() throws SQLException {
        Map<String, Item> items = new HashMap<String, Item>();
        String sql = "SELECT * FROM " + TABLE_NAME;
        Connection connection = Database.getInstance().getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            Item item = new Item();
            item.setDescription(resultSet.getString(1));
            item.setProductNumber(resultSet.getString(2));
            item.setPrice(Integer.valueOf(resultSet.getString(3)));
            item.setDiscount(Float.valueOf(resultSet.getString(4)));
            item.setImageName(resultSet.getString(5));
            items.put(item.getProductNumber(), item);
        }
        if (resultSet != null) {
            resultSet.close();
        }
        closeStatement(statement);

        return items;
    }

    /**
     * Load all of the items into the table in the database
     *
     * @param items the list of items to add to the items table
     */
    private void load(Map<String, Item> items) {
        if (items != null) {
            for (Item item : items.values()) {
                create(item);
            }
        }
        else {
            LOG.debug("Could not load items to database");
        }
    }

    /**
     * Get a 2d array representation of the list/Map for use with the JTable constructor
     *
     * @return 2D array of items
     */
    public Object[][] get2DArray() {
        Object[][] data = null;
        Map<String, Item> items;
        try {
            items = getItems();
            data = new Object[items.size()][Item.NUM_ITEM_ELEMENTS];
            int i = 0;
            for (Item item : items.values()) {
                Object[] row = item.toArray();
                data[i++] = row;
            }
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        return data;
    }
}
