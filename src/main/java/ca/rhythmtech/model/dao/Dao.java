package ca.rhythmtech.model.dao;

import ca.rhythmtech.model.Database;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Mark Doucette,
 */
public abstract class Dao {
    protected static final String DELIMITER = "\\|";
    private static Logger LOG = Logger.getLogger(Dao.class);

    private String tableName;

    /**
     * Main constructor
     *
     * @param tableName
     */
    protected Dao(String tableName) {
        this.tableName = tableName;
    }

    abstract void createTable();

    /**
     * Create the table for with the specified sql string
     *
     * @param createTableString
     * @throws SQLException
     */
    protected void createTable(String createSQL) {
        Statement statement = null;
        Connection connection = Database.getInstance().getConnection();
        try {
            statement = connection.createStatement();
            statement.executeUpdate(createSQL);
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }

    }

    /**
     * Drop the specified table of this object
     */
    public void drop() {
        Statement statement = null;
        try {
            Connection connection = Database.getInstance().getConnection();
            statement = connection.createStatement();
            statement.executeUpdate("DROP TABLE " + tableName);
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }

    }

    /**
     * Delete the specified data from the database
     *
     * @param deleteSql the sql string to execute
     * @throws SQLException
     */
    protected void delete(String deleteString) {
        Statement statement = null;
        Connection connection = Database.getInstance().getConnection();
        try {
            statement = connection.createStatement();
            statement.executeUpdate(deleteString);
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }
    }

    /**
     * Close the statement
     *
     * @param statment the statement to close
     */
    protected void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            }
            catch (SQLException e) {
                LOG.error(e.getMessage());
            }
        }

    }

}
