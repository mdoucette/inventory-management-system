package ca.rhythmtech.model.dao;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.io.StocksReader;
import ca.rhythmtech.model.Database;
import ca.rhythmtech.model.Item;
import ca.rhythmtech.model.Stock;
import ca.rhythmtech.model.Store;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This creates a dao object that is specific to Items
 *
 * @author Mark Doucette,
 */
public class StockDao extends Dao {
    public static final String TABLE_NAME = "_Stocks";
    private static final String STORE_ID = "storeId";
    private static final String ITEM_PRODUCT_NUMBER = "itemProductNumber";
    private static final String ITEM_COUNT = "itemCount";
    private static final String STOCKS_FILE = "resources/stock.txt";
    public static final String[] columnNames = {STORE_ID, ITEM_PRODUCT_NUMBER, ITEM_COUNT};
    private static final Logger LOG = Logger.getLogger(StockDao.class);

    private Map<String, Item> items;
    private Map<String, Store> stores;

    public StockDao(Map<String, Item> items, Map<String, Store> stores) {
        super(TABLE_NAME);
        this.items = items;
        this.stores = stores;
        init();
    }

    /**
     * Take care of loading / reloading the data
     *
     * @throws SQLException
     * @throws FileNotFoundException
     */
    private void init() {
        List<Stock> stocks = new ArrayList<>();
        try {
            if (!Database.tableExists(TABLE_NAME)) {
                File stocksFile = new File(STOCKS_FILE);
                if (!stocksFile.exists()) {
                    LOG.error("Stocks file not found");
                }
                else {
                    try {
                        stocks = StocksReader.read(items, stores, stocksFile);
                    }
                    catch (FileNotFoundException e) {
                        LOG.error(e.getMessage());
                    }
                    createTable(); // create a new stocks table
                    load(stocks); // populate the database with Stock rows
                }

            }
        }
        catch (ApplicationException e) {
            LOG.error(e.getMessage());
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see ca.rhythmtech.model.Dao#create()
     */
    @Override
    public void createTable() {
        String sql = String.format("CREATE TABLE %s (%s char(5), %s VARCHAR(60), %s INTEGER)", TABLE_NAME, STORE_ID, ITEM_PRODUCT_NUMBER, ITEM_COUNT);
        super.createTable(sql);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ca.rhythmtech.model.Dao#delete()
     */
    void delete(Stock stock) {
        String sql = String.format("DELETE FROM %s WHERE %s=%s AND %s=%s", TABLE_NAME, STORE_ID, stock.getStore().getID(), ITEM_PRODUCT_NUMBER, stock
                .getItem().getProductNumber());
        super.delete(sql);
    }

    /**
     * Add an item to the database.
     *
     * @param item
     */
    public void create(Stock stock) {
        String sql = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?)";
        PreparedStatement statement = null;
        Connection connection = Database.getInstance().getConnection();
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, stock.getStore().getID());
            statement.setString(2, stock.getItem().getProductNumber());
            statement.setInt(3, stock.getItemCount());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }
    }

    /**
     * Get an Stock from the database
     *
     * @return the requested item
     * @throws ApplicationException
     */
    @SuppressWarnings("resource")
    public Stock getStock(StoreDao storeDao, ItemDao itemDao, String storeId, String productNumber) throws ApplicationException {
        String sql = String.format("SELECT * FROM %s WHERE %s='%s' AND %s='%s'", TABLE_NAME, ITEM_PRODUCT_NUMBER, productNumber, STORE_ID, storeId);
        Connection connection = Database.getInstance().getConnection();
        Statement statement = null;
        Stock stock = null;
        Item item = itemDao.getItem(productNumber);
        Store store = storeDao.getStore(storeId);
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            int count = 1;
            while (resultSet.next()) {
                if (count > 1) {

                    throw new ApplicationException("Expected a single row but received " + count); // too many
                    // rows!
                }

                stock = new Stock(store, item, resultSet.getInt(3));
                count++;
            }
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }

        return stock;
    }

    /**
     * Update Stock in the database
     */
    public void update(Stock stock) {
        String sql = "update " + TABLE_NAME + " SET " + STORE_ID + "='" + stock.getStore().getID() + "', " + ITEM_PRODUCT_NUMBER + "='"
                + stock.getItem().getProductNumber() + "', " + ITEM_COUNT + "=" + stock.getItemCount() + " where " + STORE_ID + "='"
                + stock.getStore().getID() + "' AND " + ITEM_PRODUCT_NUMBER + "='" + stock.getItem().getProductNumber() + "'";
        Statement statement = null;
        try {
            statement = Database.getInstance().getConnection().createStatement();
            statement.executeUpdate(sql);
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        finally {
            closeStatement(statement);
        }
    }

    /**
     * @return the stocks
     * @throws SQLException
     * @throws ApplicationException
     */
    public List<Stock> getStocks(StoreDao storeDao, ItemDao itemDao) throws SQLException, ApplicationException {
        List<Stock> stocks = new ArrayList<Stock>();
        String sql = "SELECT * FROM " + TABLE_NAME;
        Connection connection = Database.getInstance().getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            Store store = storeDao.getStore(resultSet.getString(STORE_ID));
            Item item = itemDao.getItem(resultSet.getString(ITEM_PRODUCT_NUMBER));
            Stock stock = new Stock(store, item, resultSet.getInt(ITEM_COUNT));
            stocks.add(stock);
        }
        if (resultSet != null) {
            resultSet.close();
        }
        closeStatement(statement);

        return stocks;
    }

    /**
     * Load all of the stocks into the table in the database
     *
     * @param stocks the list of stocks to add to the stocks table
     */
    private void load(List<Stock> stocks) {
        if (stocks != null) {
            for (Stock stock : stocks) {
                create(stock);
            }
        }
        else {
            LOG.debug("Could not load items to database");
        }
    }

    /**
     * Get a 2d array representation of the list/Map for use with the JTable constructor
     *
     * @return 2D array of stocks
     * @throws ApplicationException
     */
    public Object[][] get2DArray(StoreDao storeDao, ItemDao itemDao) throws ApplicationException {
        Object[][] data = null;
        List<Stock> stocks;
        try {
            stocks = getStocks(storeDao, itemDao);
            data = new Object[stocks.size()][Stock.NUM_STOCK_ELEMENTS];
            int i = 0;
            for (Stock stock : stocks) {
                Object[] row = stock.toArray();
                data[i++] = row;
            }
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        return data;
    }
}
