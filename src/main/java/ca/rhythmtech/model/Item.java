package ca.rhythmtech.model;

import org.apache.log4j.Logger;

/**
 * Item representing an stock item in a store
 *
 * @author Mark Doucette,
 */
public class Item {
    public static final int NUM_ITEM_ELEMENTS = 5;                           // make this accesible to the
    // public
    private static Logger LOG = Logger.getLogger(Item.class);

    private String description;
    private String productNumber;
    private Integer price;
    private Float discount;
    private String imageName;

    /**
     * default constructor
     */
    public Item() {
        LOG.info("Item()");
    }

    /**
     * Constructor to initialize all fields
     *
     * @param description
     * @param productNumber
     * @param price
     * @param discount
     * @param imageName
     */
    public Item(String description, String productNumber, Integer price, Float discount, String imageName) {
        this.description = description;
        this.productNumber = productNumber;
        this.price = price;
        this.discount = discount;
        this.imageName = imageName;
    }

    /**
     * @return the lOG
     */
    public static Logger getLOG() {
        return LOG;
    }

    /**
     * @param lOG the lOG to set
     */
    public static void setLOG(Logger lOG) {
        LOG = lOG;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the productNumber
     */
    public String getProductNumber() {
        return productNumber;
    }

    /**
     * @param productNumber the productNumber to set
     */
    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    /**
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return the discount
     */
    public Float getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    /**
     * @return the imageName
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * @param imageName the imageName to set
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * Get the discounted price
     */
    public Integer getSalePrice() {
        return (int) (price * (1.0 - discount));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("Item [");
        string.append("description=" + description);
        string.append(", productNumber=" + productNumber);
        string.append(", price=" + price);
        string.append(", discount=" + discount);
        string.append(", imageName=" + imageName);
        string.append("]");

        return string.toString();
    }

    /**
     * @return an array representation of the Item
     */
    public Object[] toArray() {
        Object[] itemArray = new Object[Item.NUM_ITEM_ELEMENTS];
        int i = 0;
        itemArray[i++] = description;
        itemArray[i++] = productNumber;
        itemArray[i++] = price;
        itemArray[i++] = discount;
        itemArray[i++] = imageName;

        return itemArray;
    }

}
