package ca.rhythmtech.model;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * @author Mark Doucette,
 */
public class Database {
    private static final String DB_PROPERTIES_FILE = "props/db.properties";
    private static final String DB_DRIVER_KEY = "db.driver";
    private static final String DB_URL_KEY = "db.url";
    private static final String DB_USER_KEY = "db.user";
    private static final String DB_PASSWORD_KEY = "db.password";

    private static Database instance;
    private static Connection connection;
    private static Properties properties;
    private static Logger LOG = Logger.getLogger(Database.class);

    private boolean initialized;

    /**
     * default private constructor
     */
    private Database() {
        LOG.info("Database()");
        init();
    }

    /**
     * @return the instance of the Database. If null instantiate it
     */
    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    /**
     * The main initialization method for the database
     */
    private void init() {
        LOG.info("init()");
        if (initialized) {
            return; // we have already initialized the database
        }

        try {
            loadDBProperties();
        }
        catch (IOException e) {
            LOG.error(e.getMessage());
        }

        // continue setting up the database
        try {
            Class.forName(properties.getProperty(DB_DRIVER_KEY));
            connection = DriverManager.getConnection(properties.getProperty(DB_URL_KEY), properties.getProperty(DB_USER_KEY),
                    properties.getProperty(DB_PASSWORD_KEY));
        }
        catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        catch (ClassNotFoundException e) {
            LOG.error(e.getMessage());
        }

        initialized = true; // now we are initialized
    }

    /**
     * Shutdown the database connection
     */
    public void shutdown() {
        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {
                LOG.error(e.getMessage());
            }
        }
    }

    /**
     * Get the current connection to the database
     *
     * @return the database connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @throws IOException
     */
    private void loadDBProperties() throws IOException {
        // prepare the Properties
        File dbPropsFile = new File(DB_PROPERTIES_FILE);
        // leave if we can't find the db props file
        if (!dbPropsFile.exists()) {
            LOG.error("Database Properties file not found");
            System.exit(1);
        }

        FileInputStream dbPropsIn = new FileInputStream(dbPropsFile);

        properties = new Properties();
        properties.load(dbPropsIn);
    }

    /**
     * Check if a specific table exists in the database
     *
     * @param tableName
     * @return
     * @throws SQLException
     */
    public static boolean tableExists(String tableName) throws SQLException {
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet resultSet = databaseMetaData.getTables(connection.getCatalog(), "%", "%", null);
        String name = null;
        while (resultSet.next()) {
            name = resultSet.getString("TABLE_NAME");
            if (name.equalsIgnoreCase(tableName)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return the initialized
     */
    public boolean isInitialized() {
        return initialized;
    }

}
