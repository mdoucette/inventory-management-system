package ca.rhythmtech.model;

/**
 * Class to create a representation of a Store
 *
 * @author Mark Doucette,
 */
public class Store {
    public static final int NUM_STORE_ELEMENTS = 8;

    private String ID;
    private String description;
    private String street;
    private String city;
    private String province;
    private String postalCode;
    private String storePhone;
    private String autoPhone;

    /**
     * Default constructor
     */
    public Store() {
    }

    /**
     * Constructor to initialize the fields
     *
     * @param iD
     * @param description
     * @param street
     * @param city
     * @param province
     * @param postalCode
     * @param storePhone
     * @param autoPhone
     */
    public Store(String iD, String description, String street, String city, String province, String postalCode, String storePhone, String autoPhone) {
        ID = iD;
        this.description = description;
        this.street = street;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
        this.storePhone = storePhone;
        this.autoPhone = autoPhone;
    }

    /**
     * @return the iD
     */
    public String getID() {
        return ID;
    }

    /**
     * @param iD the iD to set
     */
    public void setID(String iD) {
        ID = iD;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the storePhone
     */
    public String getStorePhone() {
        return storePhone;
    }

    /**
     * @param storePhone the storePhone to set
     */
    public void setStorePhone(String storePhone) {
        this.storePhone = storePhone;
    }

    /**
     * @return the autoPhone
     */
    public String getAutoPhone() {
        return autoPhone;
    }

    /**
     * @param autoPhone the autoPhone to set
     */
    public void setAutoPhone(String autoPhone) {
        this.autoPhone = autoPhone;
    }

    /**
     * @return an array representation of the Store
     */
    public Object[] toArray() {
        Object[] storeArray = new Object[Store.NUM_STORE_ELEMENTS];
        int i = 0;
        storeArray[i++] = ID;
        storeArray[i++] = description;
        storeArray[i++] = street;
        storeArray[i++] = city;
        storeArray[i++] = province;
        storeArray[i++] = postalCode;
        storeArray[i++] = storePhone;
        storeArray[i++] = autoPhone;

        return storeArray;
    }

}
