package ca.rhythmtech.view;

import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Mark Doucette,
 */
@SuppressWarnings("serial")
public class StoresViewDialog extends JDialog {
    private static final int STORES_VIEW_DIALOG_WIDTH = 800;
    private static final int STORES_VIEW_DIALOG_HEIGHT = 150;
    private static final String STORES_VIEW_TITLE = "Stores View";

    private static Logger LOG = Logger.getLogger(StoresViewDialog.class);

    private JPanel contentPanel;
    private JPanel buttonPanel;
    private JPanel tablePanel;
    private JTable table;
    private JButton okayButton;
    private JButton cancelButton;

    /**
     * The main constructor for the ItemsViewDialog
     *
     * @param rowData     2d array for the JTable
     * @param columnNames 1d array for the column names
     */
    public StoresViewDialog(Object[][] rowData, String[] columnNames) {
        contentPanel = new JPanel(new BorderLayout());
        buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        tablePanel = new JPanel(new BorderLayout());

        // ///// table Panel //////////
        table = new JTable(rowData, columnNames);
        LOG.info("Stores table data loaded successfully");
        tablePanel.add(new JScrollPane(table));

        // ////// Button Panel ///////////
        okayButton = new JButton("Okay");
        okayButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                StoresViewDialog.this.dispose();

            }
        });

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                StoresViewDialog.this.dispose();

            }
        });
        buttonPanel.add(okayButton);
        buttonPanel.add(cancelButton);

        // ////////// Content Panel ///////////
        contentPanel.add(tablePanel, BorderLayout.CENTER);
        contentPanel.add(buttonPanel, BorderLayout.SOUTH);

        setTitle(STORES_VIEW_TITLE);
        setSize(new Dimension(STORES_VIEW_DIALOG_WIDTH, STORES_VIEW_DIALOG_HEIGHT));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        getContentPane().add(contentPanel);
    }

}
