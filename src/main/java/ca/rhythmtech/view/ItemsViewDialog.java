package ca.rhythmtech.view;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.model.Item;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @author Mark Doucette,
 */
@SuppressWarnings("serial")
public class ItemsViewDialog extends JDialog {
    private static final int ITEMS_VIEW_DIALOG_WIDTH = 800;
    private static final int ITEMS_VIEW_DIALOG_HEIGHT = 200;
    private static final String ITEMS_VIEW_TITLE = "Items View";

    private static Logger LOG = Logger.getLogger(ItemsViewDialog.class);

    private JPanel contentPanel;
    private JPanel buttonPanel;
    private JPanel tablePanel;
    private JPanel textPanel;
    private JTable table;
    private JButton okayButton;
    private JButton cancelButton;
    private JLabel textLabel;
    private String productNumber;

    // private boolean rowIsSelected;

    /**
     * The main constructor for the ItemsViewDialog
     *
     * @param rowData     2d array for the JTable
     * @param columnNames 1d array for the column names
     */
    public ItemsViewDialog(Object[][] rowData, String[] columnNames) {
        contentPanel = new JPanel(new BorderLayout());
        buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        tablePanel = new JPanel(new BorderLayout());
        textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        // ////// text Panel ///////////
        textLabel = new JLabel("Select a row and press 'Okay' to see the item details:");
        textPanel.add(textLabel);

        // ///// table Panel //////////
        table = new JTable(rowData, columnNames);
        LOG.info("Items table data loaded successfully");
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // set the single row selection mode for
        // the table data
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    // rowIsSelected = true;
                    int row = table.getSelectedRow();
                    productNumber = (String) table.getValueAt(row, 1); // grab the product number from the
                    // selected row
                }
            }
        });
        setPreferredColumnWidths();
        tablePanel.add(new JScrollPane(table));

        // ////// Button Panel ///////////
        okayButton = new JButton("Okay");
        okayButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (productNumber != null) { // don't try to show an item if a row hasn't been selected
                    Item item;
                    try {
                        item = MainFrame.getInstance().getMainController().getItem(productNumber);
                        showItemDialog(item);
                    }
                    catch (ApplicationException e1) {
                        LOG.error(e1.getMessage());
                    }
                    catch (IOException e1) {
                        LOG.error(e1.getMessage());
                    }
                    catch (InterruptedException e1) {
                        LOG.error(e1.getMessage());
                    }
                }
                ItemsViewDialog.this.dispose();

            }
        });

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ItemsViewDialog.this.dispose();

            }
        });
        buttonPanel.add(okayButton);
        buttonPanel.add(cancelButton);

        // ////////// Content Panel ///////////
        contentPanel.add(textPanel, BorderLayout.NORTH);
        contentPanel.add(tablePanel, BorderLayout.CENTER);
        contentPanel.add(buttonPanel, BorderLayout.SOUTH);

        setTitle(ITEMS_VIEW_TITLE);
        setSize(new Dimension(ITEMS_VIEW_DIALOG_WIDTH, ITEMS_VIEW_DIALOG_HEIGHT));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        getRootPane().setDefaultButton(cancelButton);
        getContentPane().add(contentPanel);
    }

    public void showItemDialog(Item item) throws IOException, InterruptedException {
        ItemDialog itemDialog = new ItemDialog(this, item);
        itemDialog.setLocationRelativeTo(ItemsViewDialog.this);
        itemDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        itemDialog.setVisible(true); // since the dialog is modal the rest of this won't be called until the
        // ItemDialog is disposed
        item = itemDialog.getItem();
        if (item != null) {
            MainFrame.getInstance().getMainController().updateItem(item);
        }
    }

    private void setPreferredColumnWidths() {
        TableColumn column = null;
        int columns = table.getColumnCount();

        for (int i = 0; i < columns; i++) {
            column = table.getColumnModel().getColumn(i);
            if (i == 2) {
                column.setPreferredWidth(100);
            }
            else {
                column.setPreferredWidth(30);
            }
        }
    }

}
