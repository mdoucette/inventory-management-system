package ca.rhythmtech.view;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.controller.MainController;
import ca.rhythmtech.model.Stock;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Mark Doucette,
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 150;
    private static final String FRAME_TITLE = "Inventory Management System - ";

    private static MainFrame instance;
    private static Logger LOG = Logger.getLogger(MainFrame.class);

    private MainController mainController;
    private JMenuBar menuBar;
    private JPanel contentPanel;
    JCheckBoxMenuItem descending;

    /**
     * Main constructor
     */
    private MainFrame() {
        // frame settings
        setSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        setTitle(FRAME_TITLE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        createMenuStructure();

        // content panel
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        setContentPane(contentPanel);
    }

    /**
     * @return the instance of the MainFrame
     */
    public static MainFrame getInstance() {
        if (instance == null) {
            instance = new MainFrame();
        }

        return instance;
    }

    /**
     * Create the main menu structore for the menu bar
     */
    private void createMenuStructure() {
        // menu bar
        menuBar = new JMenuBar();

        // menus
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        JMenuItem drop = new JMenuItem("Drop", KeyEvent.VK_D);
        drop.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                mainController.dropTables(); // get rid of the tables in the db
            }
        });
        JMenuItem quit = new JMenuItem("Quit", KeyEvent.VK_Q);
        quit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOG.info("System exited gracefully");
                System.exit(0); // goodbye
            }
        });
        fileMenu.add(drop);
        fileMenu.add(quit);

        JMenu tablesMenu = new JMenu("Tables");
        tablesMenu.setMnemonic(KeyEvent.VK_T);
        JMenuItem items = new JMenuItem("Items", KeyEvent.VK_I);
        items.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOG.info("ItemsViewDialog()");
                showItemsViewDialog();
            }
        });
        JMenuItem stock = new JMenuItem("Stock", KeyEvent.VK_S);
        stock.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOG.info("StocksViewDialog");
                showStocksViewDialog();
            }
        });
        JMenuItem stores = new JMenuItem("Stores", KeyEvent.VK_R);
        stores.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOG.info("StoresViewDialog");
                showStoresViewDialog();
            }
        });
        tablesMenu.add(items);
        tablesMenu.add(stock);
        tablesMenu.add(stores);

        JMenu reportsMenu = new JMenu("Reports");
        reportsMenu.setMnemonic(KeyEvent.VK_R);
        JMenu inventoryMenu = new JMenu("Inventory");
        inventoryMenu.setMnemonic(KeyEvent.VK_I);
        JMenuItem total = new JMenuItem("Total", KeyEvent.VK_T);
        total.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                showTotalInventory();

            }
        });
        descending = new JCheckBoxMenuItem("Descending");
        descending.setMnemonic('D');
        JMenuItem byPrice = new JMenuItem("By Price", KeyEvent.VK_P);
        byPrice.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                showInventoryReport("byPrice");

            }
        });
        JMenuItem byCount = new JMenuItem("By Count", KeyEvent.VK_C);
        byCount.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                showInventoryReport("byCount");

            }
        });
        JMenuItem byValue = new JMenuItem("By Value", KeyEvent.VK_V);
        byValue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                showInventoryReport("byValue");

            }
        });
        inventoryMenu.add(total);
        inventoryMenu.add(descending);
        inventoryMenu.add(byPrice);
        inventoryMenu.add(byCount);
        inventoryMenu.add(byValue);
        reportsMenu.add(inventoryMenu);

        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        JMenuItem about = new JMenuItem("About", KeyEvent.VK_A);
        about.setAccelerator(KeyStroke.getKeyStroke("F1"));
        about.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(MainFrame.this, "IMS System \nComp 2613 Assignment 2 \nBy Mark Doucette, ", "IMS System",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
        helpMenu.add(about);

        menuBar.add(fileMenu);
        menuBar.add(tablesMenu);
        menuBar.add(reportsMenu);
        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

    }

    /**
     * @return the mainController
     */
    public MainController getMainController() {
        return mainController;
    }

    /**
     * Set the main controller
     *
     * @param controller the controller to set
     */
    public void setMainController(MainController controller) {
        this.mainController = controller;
    }

    /**
     * show the ItemsViewDialog
     */
    private void showItemsViewDialog() {
        Object[][] items2dArray = mainController.getItem2DArray();
        if (items2dArray != null) {
            ItemsViewDialog dialog = new ItemsViewDialog(items2dArray, mainController.getItemColumnNames());
            dialog.setLocationRelativeTo(MainFrame.this);
            dialog.setVisible(true);
        }
        else {
            JOptionPane.showMessageDialog(MainFrame.this, "Oops,  It appears there are no Items to display. \n Please restart the program", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Show the StoresViewDialog
     */
    private void showStoresViewDialog() {
        Object[][] stores2dArray = mainController.getStore2DArray();
        if (stores2dArray != null) {
            StoresViewDialog dialog = new StoresViewDialog(stores2dArray, mainController.getStoreColumnNames());
            dialog.setLocationRelativeTo(MainFrame.this);
            dialog.setVisible(true);
        }
        else {
            LOG.debug("No stores available in database");
            JOptionPane.showMessageDialog(MainFrame.this, "Oops,  It appears there are no Stores to display. \n Please restart the program", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Show the StocksViewDialog
     */
    private void showStocksViewDialog() {
        Object[][] stocks2dArray = null;
        try {
            stocks2dArray = mainController.getStock2DArray();
        }
        catch (ApplicationException e) {
            LOG.error(e.getMessage());
        }
        if (stocks2dArray != null) {
            StocksViewDialog dialog = new StocksViewDialog(stocks2dArray, mainController.getStockColumnNames());
            dialog.setLocationRelativeTo(MainFrame.this);
            dialog.setVisible(true);
        }
        else {
            LOG.debug("No stocks available in database");
            JOptionPane.showMessageDialog(MainFrame.this, "Oops,  It appears there are no Stocks to display. \n Please restart the program", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Show the total value of the inventory in a Dialog box
     */
    private void showTotalInventory() {
        List<Stock> stocks = null;

        try {
            stocks = mainController.getStocks();
        }
        catch (SQLException | ApplicationException e) {
            LOG.error(e.getMessage());
        }
        Float totalInventory = 0.0f;
        if (stocks != null) {
            for (Stock stock : stocks) {
                totalInventory += stock.getInventoryValue();
            }
            LOG.info("Total Inventory calculated");
            JOptionPane.showMessageDialog(MainFrame.this, String.format("Total inventory value = $%8.2f", totalInventory));
        }
        else {
            LOG.debug("No total inventory to show");
            JOptionPane.showMessageDialog(MainFrame.this, "Total Inventory can't be calculated at this time", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Show the inventory report sorted accordingly
     *
     * @param sort the sort string // descending
     */
    private void showInventoryReport(String sort) {
        List<Stock> stocks = null;

        try {
            stocks = mainController.getStocks();
        }
        catch (SQLException | ApplicationException e) {
            LOG.error(e.getMessage());
        }

        if (stocks != null) {
            InventoryReportDialog dialog = new InventoryReportDialog(stocks, sort);
            dialog.setLocationRelativeTo(MainFrame.this);
            dialog.setVisible(true);
        }
        else {
            JOptionPane.showMessageDialog(MainFrame.this, "Oops, It appears there is no Inventory report", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean isDescending() {
        return descending.isSelected();
    }

}
