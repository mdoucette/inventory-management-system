package ca.rhythmtech.view;

import ca.rhythmtech.model.Stock;
import ca.rhythmtech.util.CompareByCount;
import ca.rhythmtech.util.CompareByPrice;
import ca.rhythmtech.util.CompareByValue;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * This class outputs a report of Stock in a nicely formatted list to the stdout
 *
 * @author Mark Doucette,
 */

@SuppressWarnings("serial")
public class InventoryReportDialog extends JDialog {
    private static final int INVENTORY_VIEW_DIALOG_WIDTH = 1000;
    private static final int INVENTORY_VIEW_DIALOG_HEIGHT = 250;
    private static final String INVENTORY_VIEW_TITLE = "Inventory Report View";

    private static Logger LOG = Logger.getLogger(StocksViewDialog.class);

    private JPanel contentPanel;
    private JPanel buttonPanel;
    private JPanel textAreaPanel;
    private JTextArea textArea;
    private JButton okayButton;
    private JButton cancelButton;
    private boolean descending;

    /**
     * default constructor
     */
    public InventoryReportDialog(List<Stock> stocks, String sort) {
        descending = MainFrame.getInstance().isDescending();
        stocks = sortStocks(stocks, sort, descending);
        contentPanel = new JPanel(new BorderLayout());
        buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        textAreaPanel = new JPanel(new BorderLayout());

        // ///// table Panel //////////
        LOG.info("Inventory table data loaded successfully");
        textArea = new JTextArea(400, 50);
        textArea.setText(showInventoryDetails(stocks).toString());
        textAreaPanel.add(new JScrollPane(textArea));

        // ////// Button Panel ///////////
        okayButton = new JButton("Okay");
        okayButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                InventoryReportDialog.this.dispose();

            }
        });

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                InventoryReportDialog.this.dispose();

            }
        });
        buttonPanel.add(okayButton);
        buttonPanel.add(cancelButton);

        // ////////// Content Panel ///////////
        contentPanel.add(textAreaPanel, BorderLayout.CENTER);
        contentPanel.add(buttonPanel, BorderLayout.SOUTH);

        setTitle(INVENTORY_VIEW_TITLE);
        setSize(new Dimension(INVENTORY_VIEW_DIALOG_WIDTH, INVENTORY_VIEW_DIALOG_HEIGHT));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        getContentPane().add(contentPanel);

    }

    /**
     * Show each inventory item on the stdout
     *
     * @param stocks
     * @deprecated is used here to get a Date local string
     */
    public StringBuilder showInventoryDetails(List<Stock> stocks) {

        Date currentDate = new Date();
        StringBuilder reportString = new StringBuilder();
        reportString.append("Current Date: ");

        // here we call a deprecated method to make a date string
        reportString.append(currentDate.toLocaleString() + "\n");

        reportString.append("Inventory Report\n");

        reportString.append("----------------\n");
        // SKU, Description, Price, Discount, Discounted Price, Store ID, Store Description, Item
        // Count, Inventory value
        int counter = 1;

        for (Stock stock : stocks) {
            // totalInventoryValCAD += stock.getInventoryValCAD();
            String productNumber = stock.getItem().getProductNumber();
            String description = stock.getItem().getDescription();
            int price = stock.getItem().getPrice();
            float discount = stock.getItem().getDiscount();
            float discountPrice = (float) stock.getItem().getSalePrice() / 100;
            String storeID = stock.getStore().getID();
            String storeDescription = stock.getStore().getDescription();
            int itemCount = stock.getItemCount();
            double inventoryValueCAD = stock.getInventoryValue();

            // print out the formatted stock display
            reportString.append(String.format(counter + ". %10s %45s %10.2f %5.0f%% %10.2f %10s %20s %5d %10.2f%n", productNumber, description,
                    ((float) (price) / 100), (discount * 100), discountPrice, storeID, storeDescription, itemCount, inventoryValueCAD));
            counter++;
        }

        return reportString;
    }

    /**
     * Sort the Stocks
     */
    private List<Stock> sortStocks(List<Stock> stocks, String sort, boolean descending) {
        switch (sort) {
            case "byPrice":
                CompareByPrice price = new CompareByPrice();
                price.setDescending(descending);
                Collections.sort(stocks, price);
                break;

            case "byCount":
                CompareByCount count = new CompareByCount();
                count.setDescending(descending);
                Collections.sort(stocks, count);
                break;
            case "byValue":
                CompareByValue value = new CompareByValue();
                value.setDescending(descending);
                Collections.sort(stocks, value);
                break;
            default:
                break;

        }
        return stocks;

    }
}
