package ca.rhythmtech.view;

import ca.rhythmtech.model.Item;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

/**
 * @author Mark Doucette,
 */
@SuppressWarnings("serial")
public class ItemDialog extends JDialog {
    private static final int ITEM_DIALOG_HEIGHT = 600;
    private static final int ITEM_DIALOG_WIDTH = 500;
    private static final String ITEM_WINDOW_TITLE = "Item Dialog";

    private final JPanel contentPanel = new JPanel();
    private JTextArea descriptionField;
    private JTextField priceField;
    private JTextField productNumberField;
    private JTextField discountField;
    private JTextField discountedPriceField;
    private Item item;
    private JLabel imageLabel;

    /**
     * Create the dialog.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public ItemDialog(ItemsViewDialog itemsViewDialog, Item item) throws IOException, InterruptedException {
        super(itemsViewDialog, true);
        this.item = item;

        setSize(ITEM_DIALOG_WIDTH, ITEM_DIALOG_HEIGHT);
        setTitle(ITEM_WINDOW_TITLE);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new MigLayout("", "[][][118.00][grow][grow]", "[][grow][][][][]"));
        {
            JLabel lblNewLabel = new JLabel("Product Number");
            contentPanel.add(lblNewLabel, "cell 0 0 3 1,alignx left");
        }
        {
            productNumberField = new JTextField();
            productNumberField.setEditable(false);
            contentPanel.add(productNumberField, "cell 3 0 2 1,growx");
            productNumberField.setColumns(10);
        }
        {
            JLabel lblDescription = new JLabel("Description");
            contentPanel.add(lblDescription, "cell 0 1 3 1,alignx left");
        }
        {
            descriptionField = new JTextArea();
            contentPanel.add(new JScrollPane(descriptionField), "cell 3 1 2 1,grow");
        }
        {
            JLabel lblPrice = new JLabel("Price");
            contentPanel.add(lblPrice, "cell 0 2 3 1,alignx left");
        }
        {
            priceField = new JTextField();
            contentPanel.add(priceField, "cell 3 2 2 1,growx");
            priceField.setColumns(10);
        }
        {
            JLabel lblDiscount = new JLabel("Discount");
            contentPanel.add(lblDiscount, "cell 0 3 3 1,alignx left");
        }
        {
            discountField = new JTextField();
            contentPanel.add(discountField, "cell 3 3 2 1,growx");
            discountField.setColumns(10);
        }
        {
            JLabel lblDiscountedPrice = new JLabel("Discounted Price");
            contentPanel.add(lblDiscountedPrice, "cell 0 4 3 1,alignx left");
        }
        {
            discountedPriceField = new JTextField();
            discountedPriceField.setEditable(false);
            contentPanel.add(discountedPriceField, "cell 3 4 2 1,growx");
            discountedPriceField.setColumns(10);
        }
        {
            JLabel lblImage = new JLabel("Image");
            contentPanel.add(lblImage, "cell 0 5 3 1,alignx left");
        }
        {
            imageLabel = new JLabel();
            URL imageURL = this.getClass().getResource("images/" + item.getImageName());
            // only try to set the image if the image file is found
            if (imageURL != null) {
                imageLabel.setIcon(new ImageIcon(imageURL));
            }
            contentPanel.add(imageLabel, "cell 3 5");
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                okButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setNewItemData(); // okay was pressed so load up new data into the item obj
                        ItemDialog.this.dispose();
                    }
                });
                buttonPane.add(okButton);

            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ItemDialog.this.item = null; // clear the item data
                        ItemDialog.this.dispose();
                    }
                });
                buttonPane.add(cancelButton);
                getRootPane().setDefaultButton(cancelButton);
            }
        }

        setItemDataFields(item);
    }

    /**
     * @return the item for this dialog
     */
    public Item getItem() {
        return item;
    }

    /**
     * Add the Item data to the dialog fields
     */
    public void setItemDataFields(Item item) {
        descriptionField.setText(item.getDescription());
        productNumberField.setText(item.getProductNumber());
        priceField.setText(String.format("$%6.2f", (item.getPrice() / 100.0)));
        discountField.setText(String.format("%d%%", (int) (item.getDiscount() * 100)));
        discountedPriceField.setText(String.format("$%6.2f", (item.getSalePrice() / 100.0)));
        URL imageURL = this.getClass().getResource("images/" + item.getImageName());
        // only try to set the image if the image file is found
        if (imageURL != null) {
            imageLabel.setIcon(new ImageIcon(imageURL));
        }
    }

    /**
     * Set the item to updated values
     */
    public void setNewItemData() {
        item.setDescription(descriptionField.getText());
        item.setProductNumber(productNumberField.getText());
        String price = priceField.getText();
        price = price.replaceAll("[^0-9]", "");
        if (price != null && !price.equals("")) {
            item.setPrice(Integer.parseInt(price));
        }
        else {
            showErrorPopupDialog("You must enter valid numbers");
        }
        String discount = discountField.getText();
        discount = discount.replaceAll("[^0-9.]", "");
        if (discount != null && !discount.equals("")) {
            item.setDiscount(Float.parseFloat(discount) / 100);
        }
        else {
            showErrorPopupDialog("You must enter a valid percent number");
        }

    }

    /**
     * Show a common JOptionPaneDialog popup for any errors
     */
    public void showErrorPopupDialog(String message) {
        JOptionPane.showMessageDialog(ItemDialog.this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
