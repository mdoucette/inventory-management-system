package ca.rhythmtech.controller;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.model.Database;
import ca.rhythmtech.model.Item;
import ca.rhythmtech.model.Stock;
import ca.rhythmtech.model.Store;
import ca.rhythmtech.model.dao.ItemDao;
import ca.rhythmtech.model.dao.StockDao;
import ca.rhythmtech.model.dao.StoreDao;
import ca.rhythmtech.view.MainFrame;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.List;

/**
 * This is the main controller for the entire program
 *
 * @author Mark Doucette,
 */
public class MainController {
    private static Logger LOG = Logger.getLogger(MainController.class);

    // private final Database db;
    private ItemDao itemDao;
    private StoreDao storeDao;
    private StockDao stockDao;

    /**
     * Main contructor for the Controller class
     */
    public MainController() {
        Database.getInstance().getConnection(); // light up the database
        itemDao = new ItemDao();
        storeDao = new StoreDao();

        try {
            stockDao = new StockDao(itemDao.getItems(), storeDao.getStores());
        }
        catch (SQLException e2) {
            LOG.error(e2.getMessage());

        }

        // start the gui in a separate thread
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                setNimbusLookAndFeel();
                MainFrame mainFrame = MainFrame.getInstance();
                mainFrame.setMainController(MainController.this);
                mainFrame.setVisible(true);
                mainFrame.addWindowListener(new WindowAdapter() {

                    /*
                     * (non-Javadoc)
                     * 
                     * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
                     */
                    @Override
                    public void windowClosing(WindowEvent e) {
                        // if the window is closing then make sure we terminat the connection to the db
                        Database.getInstance().shutdown();
                        System.exit(0);
                    }

                });
            }
        });

    }

    /**
     *
     */
    private void setNimbusLookAndFeel() {
        // set the Nimbus Look and feel if it is here
        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {

                try {
                    UIManager.setLookAndFeel(info.getClassName());
                }
                catch (Exception e) {
                    LOG.debug("Nimbus Look and Feel not found on system");
                }
            }
        }
    }

    /**
     * Ask the Model to drop all of the tables
     */
    public void dropTables() {
        itemDao.drop();
        storeDao.drop();
        stockDao.drop();
    }

    // ////////// Items Section /////////////

    /**
     * create item
     *
     * @param item to create
     */
    public void createItem(Item item) {
        itemDao.create(item);
    }

    /**
     * @param the index of the item in the List / Table
     * @return and Item
     * @throws ApplicationException
     */
    public Item getItem(String productNumber) throws ApplicationException {
        return itemDao.getItem(productNumber);
    }

    /**
     * Update item
     *
     * @param the item to update
     */
    public void updateItem(Item item) {
        itemDao.update(item);
    }

    /**
     * @return the ItemDAO column names
     */
    public String[] getItemColumnNames() {
        return ItemDao.columnNames;
    }

    /**
     * @return the item as a 2d array
     */
    public Object[][] getItem2DArray() {
        return itemDao.get2DArray();
    }

    // /////////// Stores Section /////////////

    /**
     * create store
     *
     * @param store to create
     */
    public void createStore(Store store) {
        storeDao.create(store);
    }

    /**
     * @param the id of the store in the Table
     * @return and store
     * @throws ApplicationException
     */
    public Store getStore(String storeId) throws ApplicationException {
        return storeDao.getStore(storeId);
    }

    /**
     * Update store
     *
     * @param the item to update
     */
    public void updateStore(Store store) {
        storeDao.update(store);
    }

    /**
     * @return the StoreDao column names
     */
    public String[] getStoreColumnNames() {
        return StoreDao.columnNames;
    }

    /**
     * @return the item as a 2d array
     */
    public Object[][] getStore2DArray() {
        return storeDao.get2DArray();
    }

    // /////////// Stocks Section /////////////

    /**
     * create stock
     *
     * @param stock to create
     */
    public void createStock(Stock stock) {
        stockDao.create(stock);
    }

    /**
     * @param the id of the stock in the Table
     * @return and stock
     * @throws ApplicationException
     */
    public Stock getStock(String storeId, String productNumber) throws ApplicationException {
        return stockDao.getStock(storeDao, itemDao, storeId, productNumber);
    }

    /**
     * get all of the stocks
     *
     * @throws ApplicationException
     * @throws SQLException
     */
    public List<Stock> getStocks() throws SQLException, ApplicationException {
        return stockDao.getStocks(storeDao, itemDao);
    }

    /**
     * Update stock
     *
     * @param the stock to update
     */

    public void updateStock(Stock stock) {
        stockDao.update(stock);
    }

    /**
     * @return the StockDao column names
     */
    public String[] getStockColumnNames() {
        return StockDao.columnNames;
    }

    /**
     * @return the item as a 2d array
     * @throws ApplicationException
     */
    public Object[][] getStock2DArray() throws ApplicationException {
        return stockDao.get2DArray(storeDao, itemDao);
    }
}
