package ca.rhythmtech.io;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.model.Store;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * @author Mark Doucette,
 */
public final class StoresReader {
    private static final int NUMBER_OF_STORES_ELEMENTS = 8;
    private static final String DELIMITER = "\\|";
    private static Logger LOG = Logger.getLogger(StoresReader.class);

    /**
     * Default constructor. Making it private prevents instances from being created
     */
    private StoresReader() {
    }

    public static Map<String, Store> read(File file) throws ApplicationException {
        // Stores
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        }
        catch (FileNotFoundException e) {
            throw new ApplicationException(e.getMessage());
        }
        List<String> storeArrayStrings = new ArrayList<String>();
        boolean afterFirst = false;
        while (scanner.hasNext()) {
            if (!afterFirst) {
                afterFirst = true;
                continue;
            }
            else {
                storeArrayStrings.add(scanner.nextLine());
            }
        }
        scanner.close();

        Map<String, Store> stores = new HashMap<>();

        // populate the stores array
        for (String storeString : storeArrayStrings) {
            String[] elements = storeString.split(DELIMITER);

            if (elements.length != NUMBER_OF_STORES_ELEMENTS) {
                throw new ApplicationException(String.format("Expected %d elements but got %d",
                        NUMBER_OF_STORES_ELEMENTS, elements.length));
            }

            Store store = new Store(elements[0], elements[1], elements[2], elements[3], elements[4], elements[5], elements[6], elements[7]);
            stores.put(store.getID(), store);

            LOG.info("Store created");
        }
        return stores;

    }
}
