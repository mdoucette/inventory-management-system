package ca.rhythmtech.io;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.Ims;
import ca.rhythmtech.model.Item;
import ca.rhythmtech.model.Stock;
import ca.rhythmtech.model.Store;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * This class parses 3 line delimited inventory lists that are passed in
 * <p/>
 * As this is a utility class we don't want an object so the constructor is private and the class is also made
 * final
 *
 * @author Mark Doucette,
 */
public final class StocksReader {

    // number of expected elements. per Stock
    private static final int NUMBER_OF_STOCK_ELEMENTS = 3;
    private static final String DELIMITER = "\\|";

    // logger
    private static Logger LOG = Logger.getLogger(Ims.class.getCanonicalName());

    /**
     * Default constructor. Making it private prevents instances from being created
     */
    private StocksReader() {
    }

    /**
     * the main parsing method to split the string into an array of separate strings. Then create an array of
     * Stock to return to the caller.
     *
     * @return an array of Item representing each item in the data set
     * @throws FileNotFoundException
     * @throws Application           exception if the number of data elements is incorrect
     */
    public static List<Stock> read(Map<String, Item> items, Map<String, Store> stores, File file) throws ApplicationException, FileNotFoundException {

        // Stocks
        Scanner inStock = new Scanner(file);
        List<String> stockArrayStrings = new ArrayList<>();
        int lineCount = 1;
        while (inStock.hasNext()) {
            if (lineCount == 1) {
                inStock.nextLine();
            }
            else {
                stockArrayStrings.add(inStock.nextLine());
            }
            lineCount++;
        }
        inStock.close();

        List<Stock> stockList = new ArrayList<>();
        for (String stockString : stockArrayStrings) {
            String[] elements = stockString.split(DELIMITER);

            if (elements.length != NUMBER_OF_STOCK_ELEMENTS) {
                throw new ApplicationException(String.format("Expected %d elements but got %d",
                        NUMBER_OF_STOCK_ELEMENTS, elements.length));
            }

            Stock stock = new Stock(stores.get(elements[0]), items.get(elements[1]), Integer.valueOf(elements[2]));
            stockList.add(stock);

            LOG.info("Stock item created");
        }

        // finally return the stockList
        return stockList;
    }
}
