package ca.rhythmtech.io;

import ca.rhythmtech.ApplicationException;
import ca.rhythmtech.model.Item;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Mark Doucette,
 */
public class ItemsReader {
    private static final String DELIMITER = "\\|";
    private static final int NUMBER_OF_ITEM_ELEMENTS = 5;
    private static Logger LOG = Logger.getLogger(ItemsReader.class);

    /**
     * Read the Items from a file on the system.
     *
     * @return a List of Items
     * @throws ApplicationException
     */
    public static Map<String, Item> read(File file) throws ApplicationException {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        }
        catch (FileNotFoundException e) {
            throw new ApplicationException(e.getMessage());
        }

        Map<String, Item> items = new HashMap<String, Item>();

        try {
            boolean afterFirst = false;
            while (scanner.hasNext()) {
                String row = scanner.nextLine();
                if (!afterFirst) {
                    afterFirst = true;
                    continue;
                }
                String[] itemString = row.split(DELIMITER);
                if (itemString.length != NUMBER_OF_ITEM_ELEMENTS) {
                    throw new ApplicationException(String.format("Expected %d elements but got %d",
                            NUMBER_OF_ITEM_ELEMENTS, itemString.length));
                }
                try {
                    String sku = itemString[1];
                    LOG.debug(sku);
                    Item item = new Item();
                    item.setDescription(itemString[0]);
                    item.setProductNumber(itemString[1]);
                    item.setPrice(Integer.valueOf(itemString[2]));
                    item.setDiscount(Float.valueOf(itemString[3]));
                    item.setImageName(itemString[4]);
                    LOG.info("Item()");
                    items.put(sku, item);
                }
                catch (NumberFormatException e) {
                    throw new ApplicationException(e.getMessage());
                }
            }
        }
        finally {
            if (scanner != null) {
                scanner.close();
            }
        }

        return items;
    }
}
