package ca.rhythmtech.util;

import ca.rhythmtech.model.Stock;

import java.util.Comparator;

/**
 * Project: _Assignment1
 * File: CompareByCount.java
 * Date: Oct 29, 2013
 * Time: 8:35:01 PM
 *
 */

/**
 * @author Mark Doucette,
 */
public class CompareByCount implements Comparator<Stock> {
    private boolean descending = false; // the default sort

    /*
     * (non-Javadoc)
     * 
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Stock stock1, Stock stock2) {
        if (descending) {
            return stock2.getItemCount() - stock1.getItemCount();
        }

        return (stock1.getItemCount() - stock2.getItemCount());
    }

    /**
     * @param descending the descending to set
     */
    public void setDescending(boolean descending) {
        this.descending = descending;
    }

}
