package ca.rhythmtech.util;

import ca.rhythmtech.model.Stock;

import java.util.Comparator;

/**
 * @author Mark Doucette,
 */
public class CompareByValue implements Comparator<Stock> {
    private boolean descending = false; // the default sort

    /*
     * (non-Javadoc)
     * 
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(Stock stock1, Stock stock2) {
        if (descending) {
            return (int) (stock2.getInventoryValue() - stock1.getInventoryValue());
        }

        return (int) (stock1.getInventoryValue() - stock2.getInventoryValue());
    }

    /**
     * @param descending the descending to set
     */
    public void setDescending(boolean descending) {
        this.descending = descending;
    }

}
