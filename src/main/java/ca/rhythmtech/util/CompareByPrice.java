package ca.rhythmtech.util;

import ca.rhythmtech.model.Stock;

import java.util.Comparator;

/**
 * This class creates an CompareByPrice object to specifically compare the prices of known Item objects. Here
 * we pass in two Item objects and use getters to extract the values for comparison
 *
 * @author Mark Doucette,
 */
public class CompareByPrice implements Comparator<Stock> {
    private boolean descending = false; // the default sort

    /**
     * Default constructor
     */
    public CompareByPrice() {
    }

    /**
     * Overide the super compare() method
     */
    @Override
    public int compare(Stock stock1, Stock stock2) {

        if (descending) {
            return (stock2.getItem().getPrice() - stock1.getItem().getPrice());
        }

        return (stock1.getItem().getPrice() - stock2.getItem().getPrice());
    }

    /**
     * Use this method to set the descending sort option for the comparator
     *
     * @param descending the descending to set
     */
    public void setDescending(boolean descending) {
        this.descending = descending;
    }
}
