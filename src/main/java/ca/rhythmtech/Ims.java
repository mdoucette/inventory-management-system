package ca.rhythmtech;

import ca.rhythmtech.controller.MainController;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Entry point into Ims
 *
 * @author Mark Doucette, 
 */
public class Ims {

    /**
     *
     */
    private static final String PROPS_LOG_PROPERTIES = "props/log.properties";
    private static Logger LOG = Logger.getLogger(Ims.class);

    /**
     * Get the MainController instantiated
     *
     * @param args
     */
    public static void main(String[] args) {
        PropertyConfigurator.configure(PROPS_LOG_PROPERTIES);

        LOG.info("Main()");

        new MainController(); // get the ball rolling
    }

}
